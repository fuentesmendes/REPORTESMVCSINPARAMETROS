﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MODELO;
using CrystalDecisions.Web;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace PRUEBAS.Controllers
{
    public class AlumnoController : Controller
    {
        PruebasEntities pe = new PruebasEntities();
        // GET: Alumno
        public ActionResult ListaAlumnos()
        {
            return View(pe.sp_listaAlumnos().ToList());
        }




        public ActionResult ExportReport()
        {
            //List allEverest = new List();

            List<sp_listaAlumnos_Result> allEverest = new List<sp_listaAlumnos_Result>();

                allEverest = pe.sp_listaAlumnos().ToList();
            

            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reportes"), "ListaAlumno.rpt"));
            rd.SetDataSource(allEverest);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            try
            {
                //Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                //stream.Seek(0, SeekOrigin.Begin);
                //return File(stream, "application/pdf", "EverestList.pdf");

                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/xls", "EverestList.xls");

                //Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.WordForWindows);
                //stream.Seek(0, SeekOrigin.Begin);
                //return File(stream, "application/doc", "EverestList.doc");
            }
            catch (Exception ex)
            {
                throw;
            }
        }


    }



}